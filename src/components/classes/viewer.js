import * as THREE from 'three'
//import { Wireframe } from 'three/examples/jsm/lines/Wireframe'
import { WireframeGeometry2 } from 'three/examples/jsm/lines/WireframeGeometry2';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import Stats from 'three/examples/jsm/libs/stats.module'
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';

export default class Viewer {
    constructor(width, height) {
        // Width and height
        this.width = width;
        this.height = height;        
        
        // Setup renderer
        this.renderer = new THREE.WebGLRenderer({antialias: true});
        this.renderer.setPixelRatio(window.devicePixelRatio)
        this.renderer.setClearColor(0x000000, 0.0)
        this.renderer.setSize(this.width, this.height);

        // Setup camera
        this.camera = new THREE.PerspectiveCamera(75, this.width / this.height, 0.1, 1000);
        this.camera.position.set(1,1,1);

        // Setup controls
        this.orbitControls = new OrbitControls(this.camera, this.renderer.domElement);

        // Setup grid
        this.grid = new THREE.GridHelper(1000, 1000);

        // Setup stats
        this.stats = new Stats();

        // Setup scene
        this.scene = new THREE.Scene();
        this.scene.add(this.grid);

        // Others
        let geo = new THREE.BoxGeometry(1,1,1, 1, 1, 1);
        //let geo = new THREE.CapsuleGeometry( 1, 1, 4, 8 );
        //let geo = new THREE.CircleGeometry( 5, 32 );
        //let geo = new THREE.RingGeometry( 1, 5, 32 );

        //const geometry = new THREE.BoxGeometry( 1, 1, 1 );        
        const geometry = new WireframeGeometry2(geo);

        //const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        const material = new LineMaterial({
            color: 0x00ff00,
            linewidth: 0.0005,
            dashed: false,
        })

        const cube = new THREE.Mesh( geometry, material );
        this.scene.add( cube );

        //this.camera.position.z = 5;
        // END Others

        // Setup Event Listeners
        window.addEventListener('resize', () => {
            this.onWindowResize();
        });

        // Main loop
        this.animate();
    }

    onWindowResize() {
        this.width = window.innerWidth;
        this.height = window.innerHeight;

        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    }

    animate() {
        const render = () => {
            requestAnimationFrame(render);

            this.orbitControls.update();
            this.renderer.render(this.scene, this.camera);
            this.stats.update();
        }
        render();        
    }    

}